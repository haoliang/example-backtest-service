import logging
from typing import Awaitable

import attrs
from attrs import field
from starlette.applications import Starlette
from starlette.routing import Route, WebSocketRoute
from starlette.websockets import WebSocket

from backtest import httpapp
from backtest.exchanges.binance.exchange import BinanceExchange
from backtest.marketdata.market import MarketData
from backtest.wsapp import WebSocketConnection

_log = logging.getLogger(__name__)


@attrs.define
class AsgiAppContext:
    marketdata: MarketData

    # TODO@haoliang should be an ABC
    exchange: BinanceExchange

    debug: bool = field(default=True)


@attrs.define
class AsgiApp:
    _ctx: AsgiAppContext

    _starlette: Starlette = field(init=False)
    __call__: Awaitable = field(init=False)

    def __attrs_post_init__(self):
        routes = [*self._ctx.exchange.rest.routes(), *httpapp.routes(), WebSocketRoute("/ws/binance/stream", self._wsapp)]

        _log.debug("routes:\n%s", LoggableRouter(routes))

        self._starlette = Starlette(
            debug=self._ctx.debug,
            routes=routes,
            exception_handlers=httpapp.exception_handlers(),
        )

        self.__call__ = self._starlette.__call__

    async def _wsapp(self, websocket: WebSocket):
        conn = WebSocketConnection(websocket, self._ctx.marketdata)
        async with conn:
            await conn.serve()


@attrs.define
class LoggableRouter:
    _routes: list[Route | WebSocketRoute]

    def __repr__(self):
        def lines():
            for r in self._routes:
                match r:
                    case Route():
                        yield "* {} {} {}".format(",".join(r.methods), r.path, r.endpoint.__qualname__)
                    case WebSocketRoute():
                        yield "* {} {}".format(r.path, r.endpoint.__qualname__)

        return "\n".join(lines())
