import time

from attrs import define

from . import facts
from .types import Interval


@define
class Clock:
    _epoch: float
    _speed: float
    _time_offset: float
    """ for ticker """
    _round_offset: dict[Interval, int]
    """ for indicators
    {Interval: round-offset} """

    @classmethod
    def from_now(cls, backtest_start_time: float, speed: float) -> "Clock":
        epoch = backtest_start_time
        wall = walltime()
        offset = wall - epoch
        rounds = {interval: absround(int(epoch), interval) for interval in facts.KLINE_INTERVALS}

        return cls(epoch, speed, offset, rounds)

    def second_time(self) -> float:
        return 1 / self._speed

    def round(self, interval: Interval) -> int:
        """round which relative to started_round"""

        try:
            offset = self._round_offset[interval]
        except KeyError as e:
            raise ValueError(f"unspected interval: {interval}") from e

        current = absround(int(self.time()), interval)

        return current - offset

    def time(self):
        elapsed = (walltime() - self._epoch - self._time_offset) * self._speed
        return self._epoch + elapsed


def walltime() -> float:
    return time.time()


def absround(timestamp: int, interval: Interval) -> int:
    assert isinstance(timestamp, int), "expects int timestamp"

    sec_per_round = int(interval.as_timedelta().total_seconds())

    return timestamp // sec_per_round
