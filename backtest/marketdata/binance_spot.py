import logging
from collections import deque

import attrs
import trio
from trio import (
    BrokenResourceError,
    ClosedResourceError,
    MemoryReceiveChannel,
    MemorySendChannel,
)

from backtest.types import Channel, Kline, KlineChannel, Peer, Ticker, TickerChannel
from backtest.clock import Clock

from .market import MarketData
from backtest import facts

_log = logging.getLogger(__name__)


@attrs.define
class BinanceSpotMarketData(MarketData):
    _clock: Clock

    _publishers: dict[Channel, MemoryReceiveChannel]
    """ known when program starts, all operations happened on Channel.clone() """

    _hists: dict[Channel, deque[Kline | Ticker]]
    """ known when program starts, deque.maxlen depends on channel, time asc (right in, left out) """

    _subscribers: dict[Channel, dict[Peer, MemorySendChannel]] = attrs.field(init=False, factory=dict)
    """ known at run-time, all operations happened on Channel.clone() """

    async def broadcast(self):
        async with trio.open_nursery() as nursery:
            for channel in self._publishers:
                _log.debug("broadcasting channel: %s", channel)
                nursery.start_soon(self._broadcast_one_channel, channel)

    async def _broadcast_one_channel(self, channel: Channel):
        async def safe_send(peer: Peer, base_sender: MemorySendChannel, event: dict):
            try:
                async with base_sender.clone() as sender:
                    await sender.send(event)
            except (BrokenResourceError, ClosedResourceError):
                del self._subscribers[channel][peer]
                _log.info("unsubscribed %s@%s", peer, channel)

        match channel:
            case TickerChannel():
                transformer = _transform_ticker_event
            case KlineChannel():
                transformer = _transform_kline_event  # type: ignore
            case _:
                raise RuntimeError("unexpected channel type")

        base_receiver = self._publishers[channel]

        async with base_receiver.clone() as receiver:
            async for _event in receiver:
                try:
                    # copy to avoid race-condition
                    subscribers = dict(self._subscribers[channel])
                except KeyError:
                    continue

                event = transformer(channel, _event)  # type: ignore

                _log.debug("broadcasting event from %s to %s", channel, subscribers.keys())
                # TODO@haoliang respects clock.second_time
                with trio.move_on_after(0.01) as cancel_scope:
                    async with trio.open_nursery() as nursery:
                        for peer, sender in subscribers.items():
                            nursery.start_soon(safe_send, peer, sender, event)

                if cancel_scope.cancel_called:
                    _log.warning("incomplete broadcast from %s to %s of %s", channel, subscribers.keys(), event)

    async def subscribe(self, channel: Channel, peer: Peer, sender: MemorySendChannel):
        if channel not in self._publishers:
            _log.debug("%s is subscribing to unknown channel %s", peer, channel)
            return

        if held_sender := self._subscribers.get(channel, {}).get(peer, None):
            assert sender == held_sender
            _log.debug("%s is re-subscribing to %s", peer, channel)
            return

        _log.debug("%s is subscribing to %s", peer, channel)

        if isinstance(channel, KlineChannel):
            _log.debug("feeding latestn %s events to peer %s", channel, peer)
            # timeout < 1m
            with trio.fail_after(50):
                await self._feed_peer_latestn_kline_events(channel, sender)

        self._subscribers.setdefault(channel, {})[peer] = sender

    async def _feed_peer_latestn_kline_events(self, channel: KlineChannel, base_sender: MemorySendChannel):
        try:
            hist: deque[Kline] = self._hists[channel]  # type: ignore
        except KeyError as e:
            raise RuntimeError("unreachable") from e

        # 35 is the minimal value, 24*2 is enough for kline-30m
        assert len(hist) >= 35, "too few klines in hist: {}".format(len(hist))

        # intended copy
        latestn = list(hist)
        _log.debug('feeding peer %d events from %s', len(latestn), channel)

        async with base_sender.clone() as sender:
            start_round = self._clock.round(channel.interval)
            for _event in latestn:
                await sender.send(_transform_kline_event(channel, _event))
                await trio.sleep(0.2)

            round_advanced = start_round != self._clock.round(channel.interval)
            if round_advanced:
                await sender.send(_transform_kline_event(channel, hist[-1]))

    async def unsubscribe(self, channel: Channel, peer: Peer):
        try:
            del self._subscribers.setdefault(channel, {})[peer]
        except KeyError:
            pass


def _transform_kline_event(channel: KlineChannel, kline: Kline) -> dict:
    """ref: https://binance-docs.github.io/apidocs/spot/en/#kline-candlestick-streams"""

    event = {
        "e": "kline",  # Event type
        "E": 123456789,  # Event time
        "s": "BNBBTC",  # Symbol
        "k": {
            "t": 123400000,  # Kline start time
            "T": 123460000,  # Kline close time
            "s": "BNBBTC",  # Symbol
            "i": "1m",  # Interval
            "f": 100,  # First trade ID
            "L": 200,  # Last trade ID
            "o": "0.0010",  # Open price
            "c": "0.0020",  # Close price
            "h": "0.0025",  # High price
            "l": "0.0015",  # Low price
            "v": "1000",  # Base asset volume
            "n": 100,  # Number of trades
            "x": False,  # Is this kline closed?
            "q": "1.0000",  # Quote asset volume
            "V": "500",  # Taker buy base asset volume
            "Q": "0.500",  # Taker buy quote asset volume
            "B": "123456",  # Ignore
        },
    }

    s = channel.symbol.join("")
    event.update({"E": kline.t * 1000, "s": s})

    k: dict = event["k"]  # type: ignore
    k.update(zip(("t", "o", "h", "l", "c", "v", "T", "q", "n", "V", "Q", "B"), map(bytes.decode, kline._raw)))
    k["t"] = int(k["t"])
    k["T"] = int(k["T"])
    k.update({"i": channel.interval.as_str(), "s": s})

    # according to https://binance-docs.github.io/apidocs/spot/en/#websocket-market-streams
    return {
        "stream": "{}@kline_{}".format(channel.symbol.join("").lower(), channel.interval.as_str()),
        "data": event,
    }


def _transform_ticker_event(channel: TickerChannel, data: Ticker) -> dict:
    """see https://binance-docs.github.io/apidocs/spot/en/#individual-symbol-mini-ticker-stream"""
    event = {
        "e": "24hrMiniTicker",  # Event type
        "E": 123456789,  # Event time
        "s": "BNBBTC",  # Symbol
        "c": "0.0025",  # Close price
        "o": "0.0010",  # Open price
        "h": "0.0025",  # High price
        "l": "0.0010",  # Low price
        "v": "10000",  # Total traded base asset volume
        "q": "18",  # Total traded quote asset volume
    }

    event.update({"E": int(data.timestamp * 1000), "s": channel.symbol.join(""), "c": str(data.last_price)})

    # according to https://binance-docs.github.io/apidocs/spot/en/#websocket-market-streams
    return {
        "stream": "{}@miniTicker".format(channel.symbol.join("").lower()),
        "data": event,
    }
