from abc import ABC

from trio import MemorySendChannel

from backtest.types import Channel, Peer


class MarketData(ABC):
    async def broadcast(self):
        raise NotImplementedError

    async def subscribe(self, channel: Channel, peer: Peer, sender: MemorySendChannel):
        raise NotImplementedError

    async def unsubscribe(self, channel: Channel, peer: Peer):
        raise NotImplementedError
