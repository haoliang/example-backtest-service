from datetime import datetime
from decimal import Decimal
from typing import Iterable

import tomli
from attrs import field, fields
import attrs

from backtest.types import Symbol


def _conv_symbols(raw: Iterable[str]):
    return set(Symbol(*s.split("-")) for s in raw)


def _conv_balances(raw: dict):
    return {currency.upper(): Decimal(balance) for currency, balance in raw.items()}


def _conv_timestr(string: str) -> int:
    return int(datetime.strptime(string, "%Y-%m-%d %H:%M:%S").timestamp())


def _bounded_playback_speed(speed: float) -> float:
    return min(60.0, max(1.0, speed))


@attrs.define
class BacktestConfig:
    serve_addr: str
    serve_port: int
    enable_kline_stream: bool
    enable_ticker_stream: bool

    exchange: str = field(converter=str.upper)
    """ enum(BINANCE, GATE, OKEX) """

    market: str = field(converter=str.upper)
    """enum(spot, future)"""

    symbols: set[Symbol] = field(converter=_conv_symbols)
    start_time: int = field(converter=_conv_timestr)
    stop_time: int = field(converter=_conv_timestr)
    playback_speed: float = field(converter=_bounded_playback_speed)
    balances: dict[str, float] = field(converter=_conv_balances)

    @classmethod
    def from_dict(cls, data: dict) -> "BacktestConfig":
        return cls(**{f.name: data[f.name] for f in fields(cls)})

    @classmethod
    def from_file(cls, file: str) -> "BacktestConfig":
        with open(file, "rb") as fp:
            data = tomli.load(fp)

        return cls.from_dict(data["backtest"])
