import logging

from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.routing import Route

from .types import BizErr

_log = logging.getLogger(__name__)


def ping(_: Request):
    return PlainTextResponse("pong")


async def debug(request: Request):
    """logs request header, path, method, query-string, payload"""

    _log.debug(
        "incoming request\n* header: %s\n* uri: %s %s\n* query: %s\n* body: %s\n* form: %s",
        request.headers,
        request.method,
        request.url,
        request.query_params,
        await request.body(),
        await request.form(),
    )

    return Response(status_code=404)


def client_http_error(_: Request, exc: HTTPException):
    _log.exception("bad request", exc_info=exc)
    return Response(exc.detail, exc.status_code, media_type="text/plain")


def biz_error(_: Request, exc: BizErr):
    _log.exception("biz error", exc_info=exc)
    return Response(exc.response, exc.status_code, media_type="application/json")


def routes():
    return [
        Route("/", ping),
        Route("/{everything:path}", debug, methods=("OPTIONS", "HEAD", "GET", "POST", "PUT", "DELETE", "PATCH")),
    ]


def exception_handlers():
    return {
        400: client_http_error,
        404: client_http_error,
        BizErr: biz_error,
    }
