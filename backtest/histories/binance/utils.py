from datetime import date as Date
from datetime import timedelta


def dates(start_date: Date, stop_date: Date):
    cursor = start_date
    one_day = timedelta(days=1)

    # 币安的daily数据从当日08:00开始
    yield start_date - one_day

    while True:
        if cursor > stop_date:
            break
        yield cursor
        cursor += one_day
