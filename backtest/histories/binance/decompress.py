import logging
import os.path
import shutil
from argparse import ArgumentParser
from concurrent.futures import ProcessPoolExecutor
from datetime import date as Date
from datetime import datetime
from pathlib import Path
from zipfile import BadZipFile, ZipFile

from attrs import define
from rich.logging import RichHandler

from backtest.histories.binance import facts, utils


@define
class Args:
    symbol: str
    start_date: Date
    stop_date: Date

    overwrite: bool

    @classmethod
    def from_args(cls, args=None) -> "Args":
        args = args or cls._parse_args()

        symbol = "{}{}".format(args.base_currency.upper(), args.quote_currency.upper())
        start_date = datetime.strptime(args.start_date, "%Y-%m-%d").date()
        stop_date = datetime.strptime(args.stop_date, "%Y-%m-%d").date()

        return cls(symbol, start_date, stop_date, args.overwrite)

    @staticmethod
    def _parse_args():
        parser = ArgumentParser()
        periods = parser.add_subparsers(dest="period", required=True)
        daily = periods.add_parser("daily")
        daily.add_argument("start_date", type=str)
        daily.add_argument("stop_date", type=str)
        daily.add_argument("base_currency", type=str)
        daily.add_argument("quote_currency", type=str)
        daily.add_argument("--overwrite", action="store_true")

        return parser.parse_args()


def unzipper(src: Path, overwrite: bool):
    logging.debug("unzipping %s", src)
    root = facts.DATA_DIR

    try:
        zip = ZipFile(src, "r")
    except BadZipFile:
        logging.exception("not a zip: %s", src)
        return
    except FileNotFoundError:
        logging.warning("not found: %s", src)
        return

    with zip:
        for file in zip.infolist():
            if file.is_dir():
                logging.debug("skipped %s, due to dir type", file.filename)
                continue

            if not file.filename.endswith(".csv"):
                logging.debug("skipped %s, due to suffix", file.filename)
                continue

            assert file.filename == f"{src.stem}.csv"

            outfile = root.joinpath(file.filename)

            if overwrite:
                outfile.unlink(missing_ok=True)

            if outfile.exists():
                logging.debug("skipped %s, which exists", file.filename)
                continue

            with zip.open(file, "r") as infp, outfile.open("wb") as outfp:
                logging.info("unzipping %s to %s", infp.name, outfp.name)
                shutil.copyfileobj(infp, outfp)


def producer(args: Args):
    basename = os.path.basename
    root = facts.DNLD_DIR

    for date in utils.dates(args.start_date, args.stop_date):
        datestr = date.strftime("%Y-%m-%d")

        for interval in facts.INTERVALS:
            yield root.joinpath(basename(facts.DAILY_KLINE_PATTERN.format(symbol=args.symbol, interval=interval, date=datestr)))

        yield root.joinpath(basename(facts.DAILY_TRADES_PATTERN.format(symbol=args.symbol, date=datestr)))


def main(args: Args):
    with ProcessPoolExecutor() as executor:
        for path in producer(args):
            executor.submit(unzipper, path, args.overwrite)


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )

    main(Args.from_args())
