import logging
from argparse import ArgumentParser

import trio
from rich.logging import RichHandler

from backtest.histories.binance import decompress, download


def _parse_args():
    parser = ArgumentParser()
    parser.add_argument("start_date", type=str, help="format: yyyy-mm-dd")
    parser.add_argument("stop_date", type=str, help="format: yyyy-mm-dd")
    parser.add_argument("base_currency", type=str, help='eg. btc')
    parser.add_argument("quote_currency", type=str, help='eg. usdt')
    parser.add_argument("--overwrite-downloaded", action="store_true")
    parser.add_argument("--overwrite-decompressed", action="store_true")

    return parser.parse_args()


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )

    args = _parse_args()

    setattr(args, "overwrite", args.overwrite_downloaded)
    trio.run(download.main, download.Args.from_args(args))
    delattr(args, "overwrite")

    setattr(args, "overwrite", args.overwrite_decompressed)
    decompress.main(decompress.Args.from_args(args))
    delattr(args, "overwrite")
