from backtest import facts as root_facts

BASE_URL = "https://data.binance.vision"
DAILY_KLINE_PATTERN = "/data/spot/daily/klines/{symbol}/{interval}/{symbol}-{interval}-{date}.zip"
DAILY_TRADES_PATTERN = "/data/spot/daily/trades/{symbol}/{symbol}-trades-{date}.zip"

DNLD_DIR = root_facts.VAR_DIR.joinpath("download", "binance")
DATA_DIR = root_facts.VAR_DIR.joinpath("data", "binance")

INTERVALS = [i.as_str() for i in root_facts.KLINE_INTERVALS]
