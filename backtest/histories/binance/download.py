"""
"<base_url>/data/spot/monthly/klines/<symbol_in_uppercase>/<interval>/<symbol_in_uppercase>-<interval>-<year>-<month>.zip"
"""

import logging
import os.path
from argparse import ArgumentParser
from datetime import date as Date
from datetime import datetime

import trio
from attrs import define
from backtest.histories.binance import facts, utils
from httpx import AsyncByteStream, AsyncClient, ReadTimeout, Timeout
from rich.logging import RichHandler
from trio import MemoryReceiveChannel, MemorySendChannel, Path


@define
class Args:
    symbol: str
    start_date: Date
    stop_date: Date

    overwrite: bool

    @classmethod
    def from_args(cls, args=None) -> "Args":
        args = args or cls._parse_args()

        symbol = "{}{}".format(args.base_currency.upper(), args.quote_currency.upper())
        start_date = datetime.strptime(args.start_date, "%Y-%m-%d").date()
        stop_date = datetime.strptime(args.stop_date, "%Y-%m-%d").date()

        return cls(symbol, start_date, stop_date, args.overwrite)

    @staticmethod
    def _parse_args():
        parser = ArgumentParser()
        periods = parser.add_subparsers(dest="period", required=True)
        daily = periods.add_parser("daily")
        daily.add_argument("start_date", type=str)
        daily.add_argument("stop_date", type=str)
        daily.add_argument("base_currency", type=str)
        daily.add_argument("quote_currency", type=str)
        daily.add_argument("--overwrite", action="store_true")

        return parser.parse_args()


async def downloader(client: AsyncClient, url: str, overwrite: bool, save_as: str = None):
    outfile = Path(facts.DNLD_DIR.joinpath(save_as or os.path.basename(url)))

    if overwrite:
        await outfile.unlink(missing_ok=True)

    if await outfile.exists():
        logging.info("%s already exist, skipped", outfile)
        return outfile

    async with client.stream("get", url) as resp:
        logging.debug("resp: %s", resp)

        if resp.status_code != 200:
            logging.warning("%s was not ok, terminated", url)
            return None

        if resp.headers.get("content-type") != "application/zip":
            logging.warning("%s was not a zip, terminated", url)
            return None

        assert isinstance(resp.stream, AsyncByteStream)

        logging.info("saving as %s", outfile)
        try:
            async with await outfile.open("wb") as fp:
                async for chunk in resp.aiter_bytes():
                    await fp.write(chunk)
        except Exception as e:
            await outfile.unlink(missing_ok=True)
            match e:
                case ReadTimeout():
                    pass
                case _:
                    logging.exception("failed to download %s", url)
            return None
        else:
            return outfile


async def producer(sender: MemorySendChannel, args: Args):
    async with sender:
        for date in utils.dates(args.start_date, args.stop_date):
            datestr = date.strftime("%Y-%m-%d")
            for interval in facts.INTERVALS:
                await sender.send(facts.DAILY_KLINE_PATTERN.format(symbol=args.symbol, interval=interval, date=datestr))
            await sender.send(facts.DAILY_TRADES_PATTERN.format(symbol=args.symbol, date=datestr))

    logging.info("production completed")


async def consumer(receiver: MemoryReceiveChannel, client: AsyncClient, args: Args, dnld_paths: list):
    async with receiver:
        async for url in receiver:
            logging.info("downloading %s", url)
            dnld_path = await downloader(client, url, args.overwrite, None)
            if dnld_path:
                dnld_paths.append(dnld_path)


async def main(args: Args):

    facts.DNLD_DIR.mkdir(parents=True, exist_ok=True)
    facts.DATA_DIR.mkdir(parents=True, exist_ok=True)

    client = AsyncClient(timeout=Timeout(60 * 5), base_url=facts.BASE_URL)
    sender, receiver = trio.open_memory_channel(0)

    # absolute paths
    dnld_paths: list[Path] = []

    async with client, receiver:
        async with trio.open_nursery() as nursery:
            nursery.start_soon(producer, sender, args)
            for _ in range(3):
                nursery.start_soon(consumer, receiver.clone(), client, args, dnld_paths)


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )

    trio.run(main, Args.from_args())
