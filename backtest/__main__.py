import logging
from argparse import ArgumentParser
from collections import deque

import hypercorn.trio
import trio
from hypercorn.config import Config as HypercornConfig
from rich.logging import RichHandler

from backtest import facts
from backtest.asgiapp import AsgiApp, AsgiAppContext
from backtest.clock import Clock
from backtest.config import BacktestConfig
from backtest.exchanges.binance.exchange import BinanceExchange
from backtest.marketdata.binance_spot import BinanceSpotMarketData
from backtest.playbackers.binance_spot_kline import (
    BinanceSpotKlinePlaybacker,
    BinanceSpotKlinePlaybackerContext,
)
from backtest.playbackers.binance_spot_ticker import (
    BinanceSpotTickerPlaybacker,
    BinanceSpotTickerPlaybackerContext,
)
from backtest.types import Kline, KlineChannel, Symbol, Ticker, TickerChannel


# TODO@haoliang a meaningful name, plz
def _init_whatever(clock: Clock):
    kline_pb = BinanceSpotKlinePlaybacker(clock=clock, contexts=[])
    ticker_pb = BinanceSpotTickerPlaybacker(clock=clock, contexts=[])
    marketdata = BinanceSpotMarketData({}, {})

    def _setup_kline_channels(symbol: Symbol):
        for interval in facts.KLINE_INTERVALS:
            channel = KlineChannel(symbol, interval)
            sender, receiver = trio.open_memory_channel(100)
            hist: deque[Kline] = deque(maxlen=facts.KLINE_HIST)

            kline_pb.contexts.append(BinanceSpotKlinePlaybackerContext(channel, sender, config.start_time, config.stop_time, hist))
            marketdata._publishers[channel] = receiver
            marketdata._hists[channel] = hist

    def _setup_ticker_channel(symbol: Symbol):
        channel = TickerChannel(symbol)
        sender, receiver = trio.open_memory_channel(100)
        hist: deque[Ticker] = deque(maxlen=facts.KLINE_HIST)

        ticker_pb.contexts.append(BinanceSpotTickerPlaybackerContext(symbol, sender, config.start_time, config.stop_time, hist))
        marketdata._publishers[channel] = receiver
        marketdata._hists[channel] = hist

    for symbol in config.symbols:
        _setup_kline_channels(symbol)
        _setup_ticker_channel(symbol)

    return kline_pb, ticker_pb, marketdata


config: BacktestConfig = None  # type: ignore
clock: Clock = None  # type: ignore
kline_playbacker: BinanceSpotKlinePlaybacker = None  # type: ignore
ticker_playbacker: BinanceSpotTickerPlaybacker = None  # type: ignore
marketdata: BinanceSpotMarketData = None  # type: ignore
exchange: BinanceExchange = None  # type: ignore
app: AsgiApp = None  # type: ignore


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("-q", "--quiet", action="store_true")
    parser.add_argument("-c", "--config-file", type=str, required=True)

    return parser.parse_args()


async def main():
    async with trio.open_nursery() as nursery:
        # TODO@haoliang makes nursery.start * 2 run concurrently
        if config.enable_kline_stream:
            await nursery.start(kline_playbacker.playback)
        if config.enable_ticker_stream:
            await nursery.start(ticker_playbacker.playback)

        logging.info('playbackers are ready')

        nursery.start_soon(marketdata.broadcast)
        nursery.start_soon(hypercorn.trio.serve, app, HypercornConfig.from_mapping({"bind": "{}:{}".format(config.serve_addr, config.serve_port)}))


if __name__ == "__main__":
    args = parse_args()

    logging.basicConfig(
        level="INFO" if args.quiet else "DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )
    logging.getLogger("multipart").setLevel(logging.WARNING)

    config = BacktestConfig.from_file(args.config_file)
    logging.info("loaded config: %s", config)

    clock = Clock.from_now(config.start_time, config.playback_speed)
    kline_playbacker, ticker_playbacker, marketdata = _init_whatever(clock)
    exchange = BinanceExchange.from_args(clock, config.balances)
    app = AsgiApp(AsgiAppContext(marketdata, exchange))

    trio.run(main)
