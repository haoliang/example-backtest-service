import logging
from collections import deque
from datetime import date as Date
from datetime import timedelta
from functools import partial
from typing import AsyncIterable, Iterable

import trio
from attrs import define, frozen
from trio import MemorySendChannel

from backtest.types import Symbol, Ticker

from .types import PlaybackerABC, SourcerABC

_log = logging.getLogger(__name__)


@frozen
class BinanceSpotTickerPlaybackerContext:
    symbol: Symbol
    sender: MemorySendChannel
    start_time: int
    stop_time: int
    hist: deque[Ticker]


Context = BinanceSpotTickerPlaybackerContext


@define(kw_only=True, slots=True)
class BinanceSpotTickerPlaybacker(PlaybackerABC):
    """
    interval
    * binance: 1000ms
    * okex: 100ms
    * gate: ?
    """

    contexts: list[Context]

    async def _ticker(self, ctx: Context):
        _log.debug("playing back ticker-%s", ctx.symbol)
        sourcer = Sourcer(symbol=ctx.symbol, start_time=ctx.start_time, stop_time=ctx.stop_time, hist=ctx.hist)
        iter = await sourcer.iterator()
        self._sema.release()

        anext = iter.__anext__  # type: ignore
        current_time = self.clock.time
        snap = partial(trio.sleep, self.clock.second_time() / 5)

        async def _ticktock():
            async with ctx.sender.clone() as sender:

                last_ticker: Ticker = None
                last_time: int = None

                while True:
                    await snap()

                    if last_ticker is None:
                        last_ticker = await anext()
                        last_time = int(last_ticker.timestamp)

                    current = int(current_time())

                    if current < last_time:
                        # wait until the trade happened
                        continue

                    await sender.send(last_ticker)
                    last_ticker = None
                    last_time = None

        try:
            await _ticktock()
        except StopAsyncIteration:
            pass

        _log.debug("end of source for ticker-%s", ctx.symbol)


@define(kw_only=True, slots=False)
class Sourcer(SourcerABC):
    symbol: Symbol
    hist: deque[Ticker]  # type: ignore

    def _dates(self):
        """币安的 daily 数据时间跨度为 当日 08:00 至次日 08:00"""

        yield Date.fromtimestamp(self.start_time) - timedelta(days=1)
        yield from super()._dates()

    def _files(self, dates: Iterable[Date]):
        """pattern: {A}{B}-trades-{yyyy}-{mm}-{dd}.csv"""

        pattern = "{}-trades-{}.csv".format(self.symbol.join(""), "{}")

        for date in dates:
            yield pattern.format(date.strftime("%Y-%m-%d"))

    @staticmethod
    def _parsed_line(line: bytes) -> Ticker:
        """see https://github.com/binance/binance-public-data#trades
        * trade Id
        * price
        * qty
        * quoteQty
        * time
        * isBuyerMaker
        * isBestMatch
        """
        cols = line.rstrip(b"\n").split(b",")
        _, price, _, _, time, *_ = cols
        return Ticker(float(price), float(time) / 1000)

    async def _fastforward(self, aiter: AsyncIterable) -> AsyncIterable:
        pre_start_time = self.start_time - 1

        ticker: Ticker
        async for ticker in aiter:
            if ticker.timestamp >= pre_start_time:
                _log.debug("binance-spot-ticker-%s stopped fast forwarding", self.symbol)
                break

        return aiter

    async def _iterator(self) -> AsyncIterable[Ticker]:
        """frequency: 1000ms/s
        see https://binance-docs.github.io/apidocs/spot/en/#individual-symbol-mini-ticker-stream
        """
        iter = super()._iterator()

        last: int = 0
        async for ticker in iter:
            traded_time = int(ticker.timestamp)
            if traded_time <= last:
                continue

            last = traded_time
            yield ticker
