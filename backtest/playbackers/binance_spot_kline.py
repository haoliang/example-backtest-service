import logging
from collections import deque
from datetime import date as Date
from datetime import timedelta
from functools import partial
from typing import AsyncIterable, Iterable

import attrs
import trio
from trio import Event, MemorySendChannel

from backtest.types import Kline, KlineChannel

from .types import PlaybackerABC, SourcerABC

_log = logging.getLogger(__name__)


@attrs.define(kw_only=True, slots=False)
class Sourcer(SourcerABC):
    channel: KlineChannel
    hist: deque[Kline]  # type: ignore

    def _dates(self):
        """币安的 daily 数据时间跨度为 当日 08:00 至次日 08:00"""

        yield Date.fromtimestamp(self.start_time) - timedelta(days=1)
        yield from super()._dates()

    def _files(self, dates: Iterable[Date]):
        """pattern: {A}{B}-{interval}-{yyyy}-{mm}-{dd}.csv"""

        pattern = "{}-{}-{}.csv".format(self.channel.symbol.join(""), self.channel.interval.as_str(), "{}")

        for date in dates:
            yield pattern.format(date.strftime("%Y-%m-%d"))

    @staticmethod
    def _parsed_line(line: bytes) -> Kline:
        """see https://github.com/binance/binance-public-data/#klines
        * Open time
        * Open
        * High
        * Low
        * Close
        * Volume
        * Close time
        * Quote asset volume
        * Number of trades
        * Taker buy base asset volume
        * Taker buy quote asset volume
        * Ignore
        """
        cols = line.rstrip(b"\n").split(b",")
        t, o, h, l, c, v, *_ = cols
        return Kline(int(t) // 1000, float(o), float(h), float(l), float(c), float(v), cols)

    async def _fastforward(self, aiter: AsyncIterable) -> AsyncIterable:
        bucket_size = self.channel.interval.as_timedelta().total_seconds()
        pre_start_bucket = int(self.start_time - bucket_size - (self.start_time % bucket_size))

        kline: Kline
        async for kline in aiter:
            if kline.t >= pre_start_bucket:
                _log.debug("binance-spot-kline-sourcer-%s stopped fast forwarding", self.channel)
                break

        return aiter


@attrs.frozen
class BinanceSpotKlinePlaybackerContext:
    channel: KlineChannel
    sender: MemorySendChannel
    start_time: int
    stop_time: int
    hist: deque[Kline]


Context = BinanceSpotKlinePlaybackerContext


@attrs.define(kw_only=True, slots=False)
class BinanceSpotKlinePlaybacker(PlaybackerABC):
    contexts: list[Context]

    sourcers: dict[KlineChannel, Sourcer] = attrs.field(init=False, factory=dict)

    async def _ticker(self, ctx: Context):
        _log.debug("playing back %s-%s", self.__class__.__name__, ctx.channel)
        sourcer = Sourcer(channel=ctx.channel, start_time=ctx.start_time, stop_time=ctx.stop_time, hist=ctx.hist)
        iter = await sourcer.iterator()
        self._sema.release()

        anext = iter.__anext__  # type: ignore
        current_round = partial(self.clock.round, ctx.channel.interval)
        snap = partial(trio.sleep, self.clock.second_time() / 5)

        async def _ticktock():
            async with ctx.sender.clone() as sender:
                last = 0
                while True:
                    await snap()

                    current = current_round()
                    elapsed = current - last
                    last = current

                    if elapsed == 0:
                        continue

                    if elapsed == 1:
                        kline = await anext()
                        _log.debug("sending round#%s to channel %s", current, ctx.channel)
                        await sender.send(kline)
                        continue

                    if elapsed > 1:
                        # for _ in range(elapsed - 1):
                        #     await anext()
                        # continue
                        raise RuntimeError(f"should not elapse more than one round; elapsed {elapsed} rounds")

                    raise RuntimeError("unreachable")

        try:
            await _ticktock()
        except StopAsyncIteration:
            _log.debug("end of source for %s", ctx.channel)
