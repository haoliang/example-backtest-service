from abc import ABC, abstractmethod
from collections import deque
from datetime import date as Date
from datetime import timedelta
from typing import AsyncIterable, ClassVar, Iterable
from functools import partial

import attrs
import trio
from trio import Path, Semaphore

from backtest import facts
from backtest.clock import Clock


@attrs.define(kw_only=True, slots=False)
class SourcerABC(ABC):
    """time asc, day by day
    see https://github.com/binance/binance-public-data
    """

    data_dir: ClassVar[Path] = Path(facts.DATA_DIR.joinpath("binance"))

    hist: ClassVar[deque] = None  # type: ignore
    """ subclass should make it as a instance property """

    start_time: int
    stop_time: int

    """ subclass should have the instance property `hist: deque[?]`
    * hist should be time asc and `right in left out`
    * element type of hist depends on channel
    """

    _paths: list[Path] = attrs.field(init=False)
    """ absolute path of data files """

    def __attrs_post_init__(self):
        self._paths = [self.data_dir.joinpath(file) for file in self._files(self._dates())]

    def _dates(self):
        """unlike python's range, both sides are included"""
        start_date = Date.fromtimestamp(self.start_time)
        stop_date = Date.fromtimestamp(self.stop_time)
        one_day = timedelta(days=1)

        cursor = start_date
        while True:
            if cursor > stop_date:
                break

            yield cursor

            cursor += one_day

    async def _ensure_all_exist(self):
        missing = []

        async def check(path: Path):
            if await path.exists():
                return
            missing.append(path)

        async with trio.open_nursery() as nursery:
            for path in self._paths:
                nursery.start_soon(check, path)

        if missing:
            raise RuntimeError("{} files were missing".format(len(missing)), missing)

    async def iterator(self) -> AsyncIterable:
        await self._ensure_all_exist()
        aiter = self._iterator()
        return await self._fastforward(aiter)

    def _files(self, dates: Iterable[Date]) -> Iterable[str]:
        """a Iterable that used to create self._pths"""
        raise NotImplementedError

    @staticmethod
    def _parsed_line(line: bytes):
        raise NotImplementedError

    async def _fastforward(self, aiter: AsyncIterable):
        """fast forward self._iterator according to self.start_time"""
        raise NotImplementedError

    async def _iterator(self) -> AsyncIterable:
        """the default iterator which yields every line of the data"""

        buf_size = 64 << 10
        parsed_line = self._parsed_line
        append_to_hist = self.hist.append

        for path in self._paths:
            async with await path.open("rb", buffering=buf_size) as fp:
                async for line in fp:
                    parsed = parsed_line(line)
                    append_to_hist(parsed)
                    yield parsed


@attrs.define(kw_only=True, slots=False)
class PlaybackerABC(ABC):
    contexts: ClassVar[list] = ()
    """ subclass should make it as a instance property """

    clock: Clock
    _sema: Semaphore = attrs.field(init=False, factory=partial(Semaphore, 0))

    async def playback(self, *, task_status=trio.TASK_STATUS_IGNORED):
        async with trio.open_nursery() as nursery:
            for ctx in self.contexts:
                nursery.start_soon(self._ticker, ctx)

            for _ in range(len(self.contexts)):
                await self._sema.acquire()

            task_status.started()

    @abstractmethod
    async def _ticker(self, ctx):
        pass
