from pathlib import Path

from . import types

KLINE_INTERVALS = [types.Interval.from_str(s) for s in ("1m", "5m", "15m", "30m")]

VAR_DIR = Path(__file__).parent.parent.joinpath("var")
DATA_DIR = VAR_DIR.joinpath("data")

ARTIFACTS_DIR = Path(__file__).parent.joinpath('artifacts')

KLINE_HIST = 50
TICKER_HIST = 3
