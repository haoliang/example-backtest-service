from abc import ABC, abstractmethod


class Account(ABC):
    pass


class Exchange(ABC):
    @property
    @abstractmethod
    def account(self) -> Account:
        pass
