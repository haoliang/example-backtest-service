# pylint: disable=W0613

import json
import logging
from decimal import Decimal, InvalidOperation
from enum import Enum

import trio
from attrs import define, field
from backtest import facts
from backtest.clock import Clock
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import Response, StreamingResponse
from starlette.routing import Route

from .account import Balance, BinanceAccount
from .info import parse_symbol_string
from .spot_orderbook import (
    BinanceSpotOrderbook,
    LimitPriceOrder,
    MarketPriceOrder,
    OrderSide,
    OrderStatus,
    OrderType,
    TimeInForce,
)
from .errors import BadSymbol

_log = logging.getLogger(__name__)


@define
class BinanceRestApp:
    """
    rest http api - binance

    * get    /api/v1/ping
    * get    /api/v3/ping
    * get    /api/v1/time
    * get    /api/v3/time
    * get    /api/v3/order
    * post   /api/v3/order
    * delete /api/v3/order
    * get    /api/v3/account
    * post   /sapi/v1/margin/transfer
    * get    /api/v3/exchangeInfo
    """

    _clock: Clock
    _account: BinanceAccount
    _spot_orderbook: BinanceSpotOrderbook

    _trans_count: int = field(init=False, default=0)

    def routes(self) -> list[Route]:
        return [
            Route("/api/v1/ping", self.get_ping, methods=["GET"]),
            Route("/api/v3/ping", self.get_ping, methods=["GET"]),
            Route("/api/v1/time", self.get_time, methods=["GET"]),
            Route("/api/v3/time", self.get_time, methods=["GET"]),
            Route("/api/v3/exchangeInfo", self.get_exchange_info, methods=["GET"]),
            Route("/sapi/v1/margin/transfer", self.post_transfer_margin, methods=["POST"]),
            Route("/api/v3/account", self.get_account, methods=["GET"]),
            Route("/api/v3/order", self.post_order, methods=["POST"]),
            Route("/api/v3/order", self.get_order, methods=["GET"]),
            Route("/api/v3/order", self.delete_order, methods=["DELETE"]),
        ]

    async def post_order(self, request: Request):
        """
        request sample:
        * header: Headers({'host': '127.0.0.1:8080', 'user-agent': 'binance/python', 'accept-encoding': 'gzip, deflate', 'accept': 'application/json', 'connection': 'keep-alive', 'x-mbx-apikey': 'bd4b1a1b5155154e2dfa9330ea8eea6b', 'content-length': '210', 'content-type': 'application/x-www-form-urlencoded'})
        * uri: POST http://127.0.0.1:8080/api/v3/order
        * query:
        * body: b'newClientOrderId=t-202205181605-m&price=8.2850&quantity=7.0&side=SELL&symbol=FILUSDT&timeInForce=GTC&timestamp=1654591641391&type=LIMIT&signature=20df1c00a1b64ea836e52ebfa1bef97483952fa6dc8a2668fe3c5 b6683f0b228'
        * form: FormData([('newClientOrderId', 't-202205181605-m'), ('price', '8.2850'), ('quantity', '7.0'), ('side', 'SELL'), ('symbol', 'FILUSDT'), ('timeInForce', 'GTC'), ('timestamp', '1654591641391'), ('type', 'LIMIT'), ('signature', '20df1c00a1b64ea836e52ebfa1bef97483952fa6dc8a2668fe3c5b6683f0b228')])

        response sample: {
            "symbol": "BTCUSDT",
            "orderId": 28,
            "orderListId": -1, //Unless OCO, value will be -1
            "clientOrderId": "6gCrw2kRUAF9CvJDGP16IP",
            "transactTime": 1507725176595,
            "price": "0.00000000",
            "origQty": "10.00000000",
            "executedQty": "10.00000000",
            "cummulativeQuoteQty": "10.00000000",
            "status": "FILLED",
            "timeInForce": "GTC",
            "type": "MARKET",
            "side": "SELL"
        }
        """

        def create_demand(form: dict):
            match form:
                case {"newClientOrderId": new_client_order_id, "price": _price, "quantity": _size, "side": _side, "symbol": _symbol, "timeInForce": _time_in_force, "type": _type}:
                    price = Decimal(_price)
                    size = Decimal(_size)
                    side = OrderSide(_side)
                    try:
                        symbol = parse_symbol_string(_symbol)
                    except ValueError:
                        raise BadSymbol

                    time_in_force = TimeInForce(_time_in_force)
                    assert time_in_force == TimeInForce.GTC

                    type = OrderType(_type)

                    match type:
                        case OrderType.LIMIT:
                            return LimitPriceOrder(symbol, side, price, size, new_client_order_id)
                        case OrderType.MARKET:
                            return MarketPriceOrder(symbol, side, price, size, new_client_order_id)
                case _:
                    raise HTTPException(400)

        try:
            demand = create_demand((await request.form())._dict)
        except (ValueError, InvalidOperation) as e:
            raise HTTPException(400) from e

        # TODO@haoliang transaction
        match demand.side:
            case OrderSide.BUY:
                self._account.spot.freeze(demand.symbol.quote, demand.quote_size)
            case OrderSide.SELL:
                self._account.spot.freeze(demand.symbol.base, demand.size)

        oe = self._spot_orderbook.create_order(demand)

        return _json_response(
            {
                "symbol": oe.demand.symbol.join(""),
                "orderId": oe.id,
                "orderListId": -1,
                "clientOrderId": oe.demand.new_client_order_id,
                "transactTime": int(oe.created_at * 1000),
                "price": oe.demand.price,
                "origQty": oe.demand.size,
                "executedQty": "0.0",
                "cummulativeQuoteQty": "0.0",
                "status": oe.status,
                "timeInForce": oe.demand.time_in_force,
                "type": oe.demand.type,
                "side": oe.demand.side,
            }
        )

    async def delete_order(self, request: Request):
        """
        request sample:
        * header: Headers({'host': '127.0.0.1:8080', 'user-agent': 'binance/python', 'accept-encoding': 'gzip, deflate', 'accept': 'application/json', 'connection': 'keep-alive', 'x-mbx-apikey': 'bd4b1a1b5155154e2dfa9330ea8eea6b', 'content-length': '179', 'content-type': 'application/x-www-form-urlencoded'})
        * uri: DELETE http://127.0.0.1:8080/api/v3/order
        * query:
        * body: b'orderId=0.1000000000000000055511151231257827021181583404541015625&symbol=BTCUSDT&timestamp=1654585751253&signature=b0c584bd3f86f43a3f83ed6c2e04859120a8ab2c6a032aa3dc2347fc8434c9bf'

        response sample: {
            "symbol": "LTCBTC",
            "origClientOrderId": "myOrder1",
            "orderId": 4,
            "orderListId": -1, //Unless part of an OCO, the value will always be -1.
            "clientOrderId": "cancelMyOrder1",
            "price": "2.00000000",
            "origQty": "1.00000000",
            "executedQty": "0.00000000",
            "cummulativeQuoteQty": "0.00000000",
            "status": "CANCELED",
            "timeInForce": "GTC",
            "type": "LIMIT",
            "side": "BUY"
        }
        """
        match (await request.form())._dict:
            case {"orderId": order_id, "symbol": _symbol}:
                pass
            case _:
                raise HTTPException(400)

        # TODO@haoliang transaction
        oe = self._spot_orderbook.cancel_order(order_id)

        match oe.demand.side:
            case OrderSide.BUY:
                self._account.spot.thaw(oe.demand.symbol.quote, oe.demand.quote_size)
            case OrderSide.SELL:
                self._account.spot.thaw(oe.demand.symbol.base, oe.demand.size)

        return _json_response(
            {
                "symbol": _symbol,
                "origClientOrderId": oe.demand.new_client_order_id,
                "orderId": oe.id,
                "orderListId": -1,
                "clientOrderId": "cancelMyOrder1",
                "price": oe.demand.price,
                "origQty": oe.demand.size,
                "executedQty": "0.00000000",
                "cummulativeQuoteQty": "0.00000000",
                "status": oe.status,
                "timeInForce": oe.demand.time_in_force,
                "type": oe.demand.type,
                "side": oe.demand.side,
            }
        )

    def get_order(self, request: Request):
        """
        request sample:
        * header: Headers({'host': '127.0.0.1:8080', 'user-agent': 'binance/python', 'accept-encoding': 'gzip, deflate', 'accept': 'application/json', 'connection': 'keep-alive', 'x-mbx-apikey': 'bd4b1a1b5155154e2dfa9330ea8eea6b'})
        * uri: GET http://127.0.0.1:8080/api/v3/order?orderId=157850387934&symbol=FILUSDT&timestamp=1654587190588&signature=698fa863cd91db23a2da019904817ec5fd720f7dec4688fdddfa6a441a19dffd
        * query: orderId=157850387934&symbol=FILUSDT&timestamp=1654587190588&signature=698fa863cd91db23a2da019904817ec5fd720f7dec4688fdddfa6a441a19dffd
        * body: b''

        response sample: {
            "symbol": "LTCBTC",
            "orderId": 1,
            "orderListId": -1, //Unless OCO, value will be -1
            "clientOrderId": "myOrder1",
            "price": "0.1",
            "origQty": "1.0",
            "executedQty": "0.0",
            "cummulativeQuoteQty": "0.0",
            "status": "NEW",
            "timeInForce": "GTC",
            "type": "LIMIT",
            "side": "BUY",
            "stopPrice": "0.0",
            "icebergQty": "0.0",
            "time": 1499827319559,
            "updateTime": 1499827319559,
            "isWorking": true,
            "origQuoteOrderQty": "0.000000"
        }
        """
        match request.query_params._dict:
            case {"orderId": order_id, "symbol": _symbol}:
                pass
            case _:
                raise HTTPException(400)

        oe = self._spot_orderbook.query_order(order_id)
        if oe.status is OrderStatus.OPEN:
            self._spot_orderbook.randomly_conclude_order(oe)
            if oe.status is OrderStatus.CLOSED:
                spot_account = self._account.spot
                match oe.demand.side:
                    case OrderSide.BUY:
                        spot_account.withdraw_from_locked(oe.demand.symbol.quote, oe.demand.quote_size)
                        spot_account.deposit(oe.demand.symbol.base, oe.demand.size)
                    case OrderSide.SELL:
                        spot_account.withdraw_from_locked(oe.demand.symbol.base, oe.demand.size)
                        spot_account.deposit(oe.demand.symbol.quote, oe.demand.quote_size)

        return _json_response(
            {
                "symbol": _symbol,
                "orderId": oe.id,
                "orderListId": -1,
                "clientOrderId": oe.demand.new_client_order_id,
                "price": oe.demand.price,
                "origQty": oe.demand.size,
                "executedQty": oe.executed_size,
                "cummulativeQuoteQty": "0.0",
                "status": oe.status,
                "timeInForce": oe.demand.time_in_force,
                "type": oe.demand.type,
                "side": oe.demand.side,
                "stopPrice": "0.0",
                "icebergQty": "0.0",
                "time": 1499827319559,
                "updateTime": 1499827319559,
                "isWorking": True,
                "origQuoteOrderQty": "0.000000",
            }
        )

    def get_account(self, request: Request):
        """
        request sample:
        * header: Headers({'host': '127.0.0.1:8080', 'user-agent': 'binance/python', 'accept-encoding': 'gzip, deflate', 'accept': 'application/json', 'connection': 'keep-alive', 'x-mbx-apikey': 'bd4b1a1b5155154e2dfa9330ea8eea6b'})
        * uri: GET http://127.0.0.1:8080/api/v3/account?timestamp=1654584766687&signature=3e3b4e633be61b320f515965c844734e240aa266021e9fe2ec51d64da3205291
        * query: timestamp=1654584766687&signature=3e3b4e633be61b320f515965c844734e240aa266021e9fe2ec51d64da3205291

        response sample: {
            "makerCommission": 15,
            "takerCommission": 15,
            "buyerCommission": 0,
            "sellerCommission": 0,
            "canTrade": true,
            "canWithdraw": true,
            "canDeposit": true,
            "updateTime": 123456789,
            "accountType": "SPOT",
            "balances": [
                { "asset": "BTC", "free": "4723846.89208129", "locked": "0.00000000" }
            ],
            "permissions": [ "SPOT" ]
        }
        """

        # TODO@haoliang scale
        # exp = Decimal('0.{:<08}'.format(''))

        def fmt_balance(currency: str, balance: Balance):
            return {"asset": currency.upper(), "free": balance.free, "locked": balance.locked}

        return _json_response(
            {
                "balances": [fmt_balance(currency, balance) for currency, balance in self._account.spot._balances.items()],
            }
        )

    async def post_transfer_margin(self, request: Request):
        """
        request sample:
        * header: Headers({'host': '127.0.0.1:8080', 'user-agent': 'binance/python', 'accept-encoding': 'gzip, deflate', 'accept': 'application/json', 'connection': 'keep-alive', 'x-mbx-apikey': 'bd4b1a1b5155154e2dfa9330ea8eea6b', 'content-length': '127', 'content-type': 'application/x-www-form-urlencoded'})
        * uri: POST http://127.0.0.1:8080/sapi/v1/margin/transfer
        * query:
        * body: b'amount=0.1&asset=USDT&timestamp=1654586679662&type=1&signature=54bea1f18b6298d09e4835f1ecfd2163e8c28622022182fa1c1dd62606a82cf9'

        response sample:
        { "tranId": 100000001 }
        """
        match (await request.form())._dict:
            case {"amount": _size, "asset": currency}:
                size = Decimal(_size)
            case _:
                raise HTTPException(400)

        self._account.transfer_spot_margin(currency, size)
        self._trans_count += 1

        return _json_response({"transId": self._trans_count})

    def get_ping(self, request: Request):
        return _json_response({})

    async def get_exchange_info(self, request: Request):
        async def source():
            fp = await trio.open_file(facts.ARTIFACTS_DIR.joinpath("binance-exchangeinfo.json"), "rb")
            chunk_size = 64 << 10

            async with fp:
                while True:
                    chunk = await fp.read(chunk_size)
                    if chunk == b"":
                        break
                    yield chunk

        return StreamingResponse(content=source(), media_type="application/json")

    def get_time(self, request: Request):
        return _json_response({"serverTime": int(self._clock.time() * 1000)})


def _json_response(data: dict, **kwargs) -> Response:
    def default(o):
        match o:
            case Decimal():
                return o.to_eng_string()
            case Enum():
                return o.value
            case _:
                raise TypeError(f"Object of type {o.__class__.__name__} is not JSON serializable")

    content = json.dumps(data, default=default)

    return Response(content, media_type="application/json", **kwargs)
