from decimal import Decimal
from typing import ClassVar

from attrs import define
from backtest.clock import Clock

from .account import BinanceAccount, SpotAccount
from .rest import BinanceRestApp
from .spot_orderbook import BinanceSpotOrderbook


@define
class BinanceExchange:
    account: BinanceAccount
    spot_orderbook: BinanceSpotOrderbook
    rest: BinanceRestApp

    fee_rate: ClassVar[Decimal] = Decimal(0.001)

    @classmethod
    def from_args(cls, clock: Clock, balances: dict):
        """
        :param balances: dict[CURRENCY, Decimal]
        """
        account = BinanceAccount(SpotAccount(), Decimal(0))
        for currency, size in balances.items():
            account.spot._balances[currency].free += size

        spot_orderbook = BinanceSpotOrderbook(clock)

        rest = BinanceRestApp(clock, account, spot_orderbook)

        return cls(account, spot_orderbook, rest)
