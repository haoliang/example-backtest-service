import json
from enum import Enum

from attrs import define, field, fields

from backtest import facts
from backtest.types import Symbol


class SymbolInfoStatus(Enum):
    """ref: https://binance-docs.github.io/apidocs/spot/en/#public-api-definitions"""

    PRE_TRADING = "PRE_TRADING"
    TRADING = "TRADING"
    POST_TRADING = "POST_TRADING"
    END_OF_DAY = "END_OF_DAY"
    HALT = "HALT"
    AUCTION_MATCH = "AUCTION_MATCH"
    BREAK = "BREAK"


@define
class SymbolInfo:
    status: SymbolInfoStatus = field(converter=SymbolInfoStatus)
    base: str = field(converter=str.upper, metadata={"key": "baseAsset"})
    base_precision: int = field(converter=int, metadata={"key": "baseAssetPrecision"})
    quote: str = field(converter=str.upper, metadata={"key": "quoteAsset"})
    quote_precision: int = field(converter=int, metadata={"key": "quoteAssetPrecision"})

    def symbol(self):
        return Symbol(self.base, self.quote)

    @classmethod
    def from_dict(cls, data: dict):
        # pylint: disable=E1133

        def src_dest():
            for f in fields(cls):
                if not f.init:
                    continue

                try:
                    json_key = f.metadata["key"]
                except KeyError:
                    yield f.name, f.name
                else:
                    yield json_key, f.name

        return cls(**{dest: data[src] for src, dest in src_dest()})


_KEYED_SIS: dict[str, SymbolInfo] = None  # type: ignore
""" {"{A}{B}": SymbolInfo} """


def parse_symbol_string(string: str) -> Symbol:
    def _load_sis():
        global _KEYED_SIS

        if _KEYED_SIS is not None:
            return _KEYED_SIS

        # $ curl -S 'http://api.binance.com/api/v3/exchangeInfo' -o binance-exchangeinfo.json
        # {
        #   timezone,
        #   rateLimits: [{rateLimitType, interval, intervalNum, limit}],
        #   symbols: [{symbol, status, baseAsset, baseAssetPrecision, quoteAsset, quotePrecision}]
        # }
        srcfile = facts.ARTIFACTS_DIR.joinpath("binance-exchangeinfo.json")

        with srcfile.open("rb") as fp:
            raw = json.load(fp)

        sis = (SymbolInfo.from_dict(data) for data in raw["symbols"])
        _KEYED_SIS = {f"{si.base}{si.quote}": si for si in sis}

        return _KEYED_SIS

    upper = string.upper()
    keyed = _load_sis()

    try:
        si = keyed[upper]
    except KeyError:
        raise ValueError(f"unknown symbol: {upper}")
    else:
        return si.symbol()
