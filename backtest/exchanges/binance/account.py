from collections import defaultdict
from decimal import Decimal
from functools import partial

import attrs
from attrs import define, field

from .errors import NotEnoughBalance


def _conv_decimal(val):
    match val:
        case Decimal():
            return val
        case int() | float() | str():
            return Decimal(val)
        case bytes():
            return Decimal(val.decode())
        case _:
            raise ValueError("unexpected type for converting to decimal. {}".format(type(val)))


@define
class Balance:
    free: Decimal = field(converter=_conv_decimal)
    locked: Decimal = field(converter=_conv_decimal)

    @classmethod
    def from_zero(cls) -> "Balance":
        return cls(Decimal(0.0), Decimal(0.0))

    @property
    def total(self) -> Decimal:
        return self.free + self.locked


@attrs.define
class SpotAccount:
    _balances: dict[str, Balance] = field(init=False, factory=partial(defaultdict, Balance.from_zero))
    """ dict[currency, balance] """

    def freeze(self, currency: str, size: Decimal):
        assert size > 0

        balance = self._balances[currency]
        new_free = balance.free - size

        if new_free < 0:
            raise NotEnoughBalance

        balance.free = new_free
        balance.locked += size

    def thaw(self, currency: str, size: Decimal):
        assert size > 0

        balance = self._balances[currency]

        new_locked = balance.locked - size

        if new_locked < 0:
            raise RuntimeError("unreachable")

        balance.locked = new_locked
        balance.free += size

    def withdraw(self, currency: str, size: Decimal):
        assert size > 0

        balance = self._balances[currency]
        new_free = balance.free - size

        if new_free < 0:
            raise NotEnoughBalance

        balance.free = new_free

    def deposit(self, currency: str, size: Decimal):
        assert size > 0

        balance = self._balances[currency]
        balance.free += size

    def withdraw_from_locked(self, currency: str, size: Decimal):
        assert size > 0

        balance = self._balances[currency]
        new_locked = balance.locked - size

        if new_locked < 0:
            raise NotEnoughBalance

        balance.locked = new_locked


@attrs.define
class BinanceAccount:
    spot: SpotAccount
    """ dict[currency, balance] """

    _margin_balance: Decimal = field(converter=_conv_decimal)

    def transfer_spot_margin(self, currency: str, size: Decimal):
        assert size > 0

        self.spot.withdraw(currency, size)
        self._margin_balance += size
