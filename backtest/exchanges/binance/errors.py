from backtest.types import BizErr

# service/logic error
# ref: https://binance-docs.github.io/apidocs/spot/en/#error-codes


class NotEnoughBalance(BizErr, code=-3041, phrase="BALANCE_NOT_CLEARED"):
    pass


class BadSymbol(BizErr, code=-1121, phrase="BAD_SYMBOL"):
    pass
