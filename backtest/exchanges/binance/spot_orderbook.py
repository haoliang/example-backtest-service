import random
from decimal import Decimal
from enum import Enum
from typing import ClassVar
from uuid import uuid4

import attrs
from attrs import define, field
from backtest.clock import Clock
from backtest.types import Symbol
from starlette.exceptions import HTTPException


class OrderStatus(Enum):
    OPEN = "NEW"
    CLOSED = "FILLED"
    CANCELLED = "CANCELLED"


class OrderSide(Enum):
    BUY = "BUY"
    SELL = "SELL"


class OrderType(Enum):
    LIMIT = "LIMIT"
    MARKET = "MARKET"


class TimeInForce(Enum):
    GTC = "GTC"
    IOC = "IOC"
    FOK = "FOK"


@define
class OrderDemand:
    type: ClassVar[OrderType]
    time_in_force: ClassVar[TimeInForce] = TimeInForce.GTC

    symbol: Symbol
    side: OrderSide
    price: Decimal
    size: Decimal

    quote_size: Decimal = field(init=False)

    def __attrs_post_init__(self):
        self.quote_size = self.price * self.size


@define
class LimitPriceOrder(OrderDemand):
    type: ClassVar[OrderType] = OrderType.LIMIT

    new_client_order_id: str


@define
class MarketPriceOrder(OrderDemand):
    type: ClassVar[OrderType] = OrderType.MARKET

    # TODO@haoliang price?

    new_client_order_id: str


@define
class OrderExecution:
    id: str
    demand: LimitPriceOrder | MarketPriceOrder
    executed_size: Decimal
    status: OrderStatus
    created_at: float
    updated_at: float


@attrs.define
class BinanceSpotOrderbook:
    _clock: Clock

    _table: dict[str, OrderExecution] = field(init=False, factory=dict)

    # # TODO@haoliang should be a price/time priority queue
    # _asks: PriorityQueue[OrderExecution] = field(init=False, factory=PriorityQueue)
    # _bids: PriorityQueue[OrderExecution] = field(init=False, factory=PriorityQueue)

    def create_order(self, demand: LimitPriceOrder | MarketPriceOrder):
        id = uuid4().hex
        now = self._clock.time()
        o = OrderExecution(id, demand, Decimal(0.0), OrderStatus.OPEN, now, now)
        self._table[o.id] = o
        return o

    def cancel_order(self, order_id: str) -> OrderExecution:
        try:
            oe = self._table.pop(order_id)
        except KeyError:
            raise HTTPException(404, f"no such order: {order_id}")

        oe.status = OrderStatus.CANCELLED
        oe.updated_at = self._clock.time()

        return oe

    def query_order(self, order_id: str):
        try:
            return self._table[order_id]
        except KeyError:
            raise HTTPException(404, f"no such order: {order_id}")

    def randomly_conclude_order(self, order: OrderExecution):
        # TODO@haoliang conclude?
        # randomly: depends on query times, created time ...
        # evidenced: kline, trades
        concluded = random.random() < 1.1

        if concluded:
            self.conclude_order(order)

    def conclude_order(self, order: OrderExecution):
        try:
            del self._table[order.id]
        except KeyError:
            raise HTTPException(404, f"no such order: {order.id}")

        order.status = OrderStatus.CLOSED
        order.updated_at = self._clock.time()
        order.executed_size = order.demand.size
