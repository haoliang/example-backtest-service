import json
from datetime import timedelta
from enum import Enum

from attrs import field, frozen


@frozen
class Kline:
    t: int
    """ open time (unix timestamp) """
    o: float
    """ open price """
    h: float
    """ high price """
    l: float
    """low price"""
    c: float
    """close price"""
    v: float
    """volume"""

    _raw: list[bytes] = field(default=None)


@frozen
class Ticker:
    last_price: float
    """last traded price"""
    timestamp: float
    """ last traded timestamp (unix timestamp) """

    _raw: list[bytes] = field(default=None)


class IntervalUnit(Enum):
    SECOND = "seconds"
    MINUTE = "minutes"
    HOUR = "hours"

    @classmethod
    def from_char(cls, char: str) -> "IntervalUnit":
        match char:
            case "m":
                return cls.MINUTE
            case "s":
                return cls.SECOND
            case "h":
                return cls.HOUR
            case _:
                raise ValueError(f"unexpected unit: {char}")

    def as_char(self) -> str:
        match self:
            case self.SECOND:
                return "s"
            case self.MINUTE:
                return "m"
            case self.HOUR:
                return "h"
            case _:
                raise RuntimeError("unreachable")


@frozen
class Interval:
    val: int
    unit: IntervalUnit

    def as_timedelta(self):
        return timedelta(**{self.unit.value: self.val})

    def __str__(self):
        return self.as_str()

    def as_str(self):
        return "{}{}".format(self.val, self.unit.as_char())

    @classmethod
    def from_str(cls, string: str):
        val = int(string[:-1])
        unit = IntervalUnit.from_char(string[-1])

        return cls(val, unit)


@frozen
class Symbol:
    base: str = field(converter=str.upper)  # type: ignore
    """ base currency; upper; eg, BTC """
    quote: str = field(converter=str.upper)  # type: ignore
    """ quote currency; upper; eg, USDT """

    def join(self, separator: str):
        return f"{self.base}{separator}{self.quote}"

    def __str__(self):
        return self.join("-")


class Channel:
    pass


@frozen
class KlineChannel(Channel):
    symbol: Symbol
    interval: Interval

    def __repr__(self):
        return "kline-{}-{}".format(self.symbol, self.interval)


@frozen
class TickerChannel(Channel):
    symbol: Symbol

    def __repr__(self):
        return "ticker-{}".format(self.symbol)


@frozen
class Peer:
    """websocket peer"""

    ip: str
    port: int

    def __str__(self):
        return "{}:{}".format(self.ip, self.port)

    __repr__ = __str__


class UnnamedError(Exception):
    """errors which need to be more specific"""


class BizErr(Exception):
    status_code = 400
    response = ""

    def __init_subclass__(cls, /, code: int, phrase: str, status_code: int = 400, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.response = json.dumps({"code": code, "msg": phrase})
        cls.status_code = status_code
