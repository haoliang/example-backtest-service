import logging
from contextlib import AsyncExitStack
from typing import Iterable

import trio
from attrs import define, field
from starlette.websockets import WebSocket, WebSocketDisconnect, WebSocketState
from trio import MemoryReceiveChannel, MemorySendChannel, Nursery

from .marketdata.market import MarketData
from .types import Channel, Interval, KlineChannel, Peer, TickerChannel
from .exchanges.binance.info import parse_symbol_string

# todo inject request/connection id via contextvars
_log = logging.getLogger(__name__)


@define
class WebSocketConnection:
    _conn: WebSocket
    _market: MarketData

    _peer: Peer = field(init=False)
    _channels: list[Channel] = field(init=False)

    _sender: MemorySendChannel = field(init=False)
    _receiver: MemoryReceiveChannel = field(init=False)

    _exit_stack: AsyncExitStack = field(init=False)

    def __attrs_post_init__(self):
        self._peer = Peer(*self._conn.scope["client"])
        self._channels = list(parsed_streams(self._conn.scope["query_string"].decode()))

        self._sender, self._receiver = trio.open_memory_channel(0)
        self._exit_stack = AsyncExitStack()

    async def __aenter__(self):
        await self._exit_stack.enter_async_context(self._sender)
        await self._exit_stack.enter_async_context(self._receiver)

    async def __aexit__(self, *_):
        await self._unsubscribe()
        await self._exit_stack.aclose()

    async def serve(self):
        async with self._sender, self._receiver:
            nursery: Nursery
            try:
                async with trio.open_nursery() as nursery:
                    await nursery.start(self._accept)
                    nursery.start_soon(self._read_and_discard)
                    nursery.start_soon(self._forward)
            except WebSocketDisconnect:
                pass

    async def _subscribe(self):
        """structure of self._conn.scope
        {
            "type": "websocket",
            "asgi": {"spec_version": "2.3", "version": "3.0"},
            "scheme": "ws",
            "http_version": "1.1",
            "path": "/ws",
            "raw_path": b"/ws",
            "query_string": b"streams=a@b/c@d",
            "root_path": "",
            "headers": [(b"host", b"127.0.0.1:8080"), (b"connection", b"Upgrade"), (b"upgrade", b"websocket"), (b"sec-websocket-version", b"13"), (b"sec-websocket-key", b"Y9eJi/NOlM92xLGsKTzLDg==")],
            "client": ("127.0.0.1", 51828),
            "server": ("127.0.0.1", 8080),
            "subprotocols": [],
            "extensions": {"websocket.http.response": {}},
            "app": "starlette.applications.Starlette",
            "router": "starlette.routing.Router",
            "endpoint": "function ws_entrypoint",
            "path_params": {},
        }
        """

        for channel in self._channels:
            await self._market.subscribe(channel, self._peer, self._sender)

    async def _unsubscribe(self):
        for channel in self._channels:
            await self._market.unsubscribe(channel, self._peer)

    async def _read_and_discard(self):
        conn = self._conn
        assert conn.application_state == WebSocketState.CONNECTED and conn.client_state == WebSocketState.CONNECTED
        receive = conn.receive_text

        while True:
            msg = await receive()
            _log.debug("recv msg: %s", msg)

    async def _forward(self):
        send_json = self._conn.send_json

        async with self._receiver.clone() as receiver:
            async for event in receiver:
                await send_json(event)

    async def _accept(self, task_status=trio.TASK_STATUS_IGNORED):
        await self._conn.accept()
        task_status.started()
        await self._subscribe()


def parsed_streams(string: str) -> Iterable[Channel]:
    """see: https://binance-docs.github.io/apidocs/spot/cn/#websocket
    * 组合streams的URL格式为 /stream?streams=<streamName1>/<streamName2>/<streamName3>
    * stream名称中所有交易对均为小写
    stream names:
    * <symbol>@kline_<interval>
    * <symbol>@miniTicker
    interval: {1,5,15,30}m
    symbol: {a}{b}
    """
    # TODO@haoliang maybe use robust urllib3.parse_url instead?
    assert string.startswith("streams=")

    for s in string[len("streams=") :].split("/"):
        _symbol, _rest = s.split("@", 1)

        # TODO@haoliang should be exchange-agnostic
        symbol = parse_symbol_string(_symbol)

        match _rest.split("_"):
            case ["kline", _interval]:
                yield KlineChannel(symbol, Interval.from_str(_interval))
            case ["miniTicker"]:
                yield TickerChannel(symbol)
            case _:
                raise ValueError(f"unexpected channel: {_rest}")
