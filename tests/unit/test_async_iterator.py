from typing import AsyncIterable

import pytest


async def iterator(n: int):
    for i in range(n):
        yield i


async def anext(it: AsyncIterable):
    async for i in it:
        return i


async def test_anext():
    n = 3

    it = iterator(n)

    for i in range(n):
        assert await anext(it) == i

    assert await anext(it) is None


async def test_dunder_anext():
    n = 3

    it = iterator(n)

    assert hasattr(it, "__anext__")

    for i in range(n):
        assert await it.__anext__() == i

    with pytest.raises(StopAsyncIteration):
        await it.__anext__()
