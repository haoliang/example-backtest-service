import time
from datetime import datetime

from backtest.clock import Clock
from backtest.types import Interval


def test_clock_apis():
    clock = Clock.from_now(int(datetime(2022, 5, 1, 0, 0, 0).timestamp()), 60)

    assert clock.round(Interval.from_str("1m")) == 0
    time.sleep(1)
    assert clock.round(Interval.from_str("1m")) == 1

    assert clock.time() >= clock._epoch + 60
