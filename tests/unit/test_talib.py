from itertools import islice
from typing import Iterable

import numpy as np
import talib
from backtest.types import Kline


def compute_latest_2_macd(klines: list[Kline]):
    """time asc"""

    def close_prices(klines: Iterable[Kline]):
        def _source():
            """ensure times of klines are in asc order"""
            prev = 0
            for kline in klines:
                assert kline.t >= prev
                yield kline.c

        return np.array(list(_source()), float)

    def compute(closes: np.array):
        assert len(closes) >= 35

        macd: np.ndarray
        signal: np.ndarray
        hist: np.ndarray
        macd, signal, hist = talib.MACD(
            closes, fastperiod=12, slowperiod=26, signalperiod=9
        )
        _ = hist
        assert all(map(np.isnan, macd[:33])) and all(
            map(lambda i: not np.isnan(i), macd[33:])
        )
        assert all(map(np.isnan, signal[:33])) and all(
            map(lambda i: not np.isnan(i), signal[33:])
        )

        return [macd[-1] - signal[-1], macd[-2] - signal[-2]]

    closes = close_prices(klines)

    return compute(closes)


def test_compute_1m_macd(binance_1m_klines: Iterable[Kline]):
    diff = compute_latest_2_macd(islice(binance_1m_klines, 0, 50))

    assert diff == [0.08081601605002228, 0.26930980745161304]


def test_compute_5m_macd(binance_5m_klines: Iterable[Kline]):
    diff = compute_latest_2_macd(islice(binance_5m_klines, 0, 50))

    assert diff == [-1.7068582448287541, -1.3575506062967975]


def test_compute_15m_macd(binance_15m_klines: Iterable[Kline]):
    diff = compute_latest_2_macd(islice(binance_15m_klines, 0, 50))

    assert diff == [-2.451501669075096, -2.3947737534673923]


def test_compute_30m_macd(binance_30m_klines: Iterable[Kline]):
    diff = compute_latest_2_macd(islice(binance_30m_klines, 0, 50))

    assert diff == [-0.2048738828977843, 0.9708410212282015]
