from pathlib import Path
from typing import Iterable

import pytest
from backtest.playbackers.binance_spot_kline import Sourcer
from backtest.types import Kline

EXIT = object()


def _binance_kline_loader(interval: str):
    line_parser = Sourcer._parsed_line
    data_dir = Path(__file__).parent.parent.joinpath("data")
    path = data_dir.joinpath(f"ETHUSDT-{interval}-2022-05-25.csv")

    with open(path, "rb") as fp:
        for line in fp:
            yield line_parser(line)


@pytest.fixture
def binance_1m_klines():
    g = _binance_kline_loader("1m")
    try:
        yield g
    finally:
        g.close()


@pytest.fixture
def binance_5m_klines():
    g = _binance_kline_loader("5m")
    try:
        yield g
    finally:
        g.close()


@pytest.fixture
def binance_15m_klines():
    g = _binance_kline_loader("15m")
    try:
        yield g
    finally:
        g.close()


@pytest.fixture
def binance_30m_klines():
    g = _binance_kline_loader("30m")
    try:
        yield g
    finally:
        g.close()
