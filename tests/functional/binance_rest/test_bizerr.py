from httpx import AsyncClient


async def test_notfound(client: AsyncClient):
    resp = await client.get("/api/v3/order", params={"orderId": "notexistsone", "symbol": "BTCUSDT"})
    assert resp.headers["content-type"].startswith("text/plain")
    assert resp.status_code == 404


async def test_badrequest(client: AsyncClient):
    resp = await client.get("/api/v3/order", params={})
    assert resp.headers["content-type"].startswith("text/plain")
    assert resp.status_code == 400


async def test_badsymbol(client: AsyncClient):
    resp = await client.post(
        "/api/v3/order",
        data={
            "newClientOrderId": "t-202205181605-m",
            "price": "8.2850",
            "quantity": "7.0",
            "side": "SELL",
            "symbol": "FILUSDTXXX",
            "timeInForce": "GTC",
            "type": "LIMIT",
        },
    )
    assert resp.status_code == 400
    assert resp.headers["content-type"] == "application/json"
    assert resp.content == b'{"code": -1121, "msg": "BAD_SYMBOL"}'
