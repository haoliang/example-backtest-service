import pytest
from httpx import AsyncClient


@pytest.fixture(scope="module")
def server_port():
    # TODO@haoliang could be random
    return 8080


@pytest.fixture(scope="function")
async def client(server_port: int):
    async with AsyncClient(base_url=f"http://127.0.0.1:{server_port}") as c:
        yield c
