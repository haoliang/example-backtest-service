from httpx import AsyncClient


async def test_ping(client: AsyncClient):
    for path in ("/api/v1/ping", "/api/v3/ping"):
        resp = await client.get(path)
        assert resp.is_closed
        assert resp.status_code == 200
        assert resp.content == b"{}"


async def test_time(client: AsyncClient):
    for path in ("/api/v1/time", "/api/v3/time"):
        resp = await client.get(path)
        assert resp.status_code == 200
