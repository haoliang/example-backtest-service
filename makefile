.PHONY: test
.DEFAULT_GOAL: install-dev

install-dev: venv
	venv/bin/pip install -r deps.dev

install: venv
	venv/bin/pip install -r deployment/deps.prod

venv:
	python3 -m venv venv

create-conda-env:
	conda env create -f deployment/conda-environment.yml

test:
	venv/bin/pytest -vv tests/unit

test-functional:
	venv/bin/pytest -vv tests/functional

lint:
	venv/bin/pylint -j $$(nproc) --py-version 3.10 --exit-zero backtest
	pyright backtest --venv-path venv --pythonplatform Linux --pythonversion 3.10 || true

run:
	venv/bin/python -m backtest -c binance-spot.toml
